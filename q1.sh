#!/bin/bash

while true; do

	echo "1 - verificar se o usuario existe"
	echo "2 - vericicar se o usuario esta logado na maquina"
	echo "3 - lista de arquivos da pasta home do usuario"
	echo "4 - sair"

	read -p "escolha uma opcão: " op

	case $op in
		1) 
			read -p "usuario: " u
			id  "$u" &>/dev/null
			[ $? -eq 0 ] && echo "existe" || echo "n exite"
			;;


		2)
			read -p "usuario: " a 
		       	id "$a" &>/dev/null 
		       	if [ $? -eq 0 ]; then
				r=$(who | grep -w "$a")
				if [ -n "$r" ]; then
					echo "usuario esta logado"
				else 
					echo "usuario nao esta logado"
				fi
			else 
				echo "usuario n existe"
				exit 1
			fi	
			;;

		
		3)
			read -p "usuario: " b
			id "$b" &>/dev/null
			if [ $? -eq 0 ]; then
				if [ -d "/home/$b" ]; then
					ls "/home/$b"
				else 
					echo " a pasta home desse usuario n existe"
					exit 1
				fi
			else 
				echo " usuario n existe "
				exit 1

			fi
			;;

		4)exit 0;;

	esac
done
