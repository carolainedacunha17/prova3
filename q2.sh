#!/bin/bash

arql="lista.txt"
arqr="resultado.txt"

> "$arqr"

while IFS= read -r arq; do
	if [ -f "$arq" ]; then
		hash=$(md5sum "$arq")
		hash="${hash%% *}"
		echo -e "$hash\t$arq" >> "$arqr"
	else
		echo " arquvio n escontrado: $arq"

	fi
done < "$arql"

echo "arquivo salvo em $arqr"
